<?php
namespace T3easy\IndexedSearchAbsrefprefix\Controller;

class SearchFormController extends \TYPO3\CMS\IndexedSearch\Controller\SearchFormController
{
    /**
     * Just return true because its deprecated since TYPO3 CMS 7, will be removed in TYPO3 CMS 8
     * (it is not used in the core any more, see #44381)
     *
     * @param array $row Result row array
     * @return bool Returns TRUE if record is still available
     */
    public function checkExistance($row)
    {
        return true;
    }

}
