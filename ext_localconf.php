<?php
defined('TYPO3_MODE') or die();

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['tx_indexedsearch'] = [
    'className' => 'T3easy\\IndexedSearchAbsrefprefix\\Controller\\SearchFormController'
];

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\IndexedSearch\\Controller\\SearchFormController'] = [
    'className' => 'T3easy\\IndexedSearchAbsrefprefix\\Controller\\SearchFormController'
];
