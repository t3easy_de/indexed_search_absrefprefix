<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Indexed search absRefPrefix patch',
    'description' => 'Work around https://forge.typo3.org/issues/50095',
    'category' => 'misc',
    'author' => 'Jan Kiesewetter',
    'author_email' => 'jan@t3easy.de',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.10-6.2.99',
            'indexed_search' => ''
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
);
